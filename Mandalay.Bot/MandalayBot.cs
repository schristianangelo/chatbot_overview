// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Mandalay.Bot.Models;
using Mandalay.LUIS;
using Mandalay.QnAMaker;
using Mandalay.QnAMaker.Models;
using Mandalay.SAP;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Dialogs.Choices;
using Microsoft.Bot.Schema;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Microsoft.BotBuilderSamples
{
    /// <summary>
    /// Main entry point and orchestration for bot.
    /// </summary>
    public class MandalayBot : IBot
    {
        // Supported LUIS Intents
        public const string Inquiry = nameof(Inquiry);
        public const string FAQ = nameof(FAQ);

        /// <summary>
        /// Key in the bot config (.bot file) for the LUIS instance.
        /// In the .bot file, multiple instances of LUIS can be configured.
        /// </summary>
        public static readonly string LuisConfiguration = "MandalayLuisBot";
        private readonly BotServices _services;
        private static readonly HttpClient _httpClient = new HttpClient();


        private static readonly string qnaMakerApiUrl = "https://workshop-bot.azurewebsites.net";
        private static readonly string qnaMakerApiKey = "44fbee04-bbe7-4485-9c6b-c22684589b8d";
        private static readonly string qnaMakerKnowledgeBaseId = "532ca894-8e65-4281-a0ed-a9e02e62068b";

        private static readonly QnAMakerClient qna = new QnAMakerClient(qnaMakerApiUrl, qnaMakerApiKey, qnaMakerKnowledgeBaseId);

        private static readonly string luisAppId = "7343ba72-daed-4c15-a523-bec14186bbfd";
        private static readonly string luisSubscriptionKey = "bcb7183be7bb4ab6b958887c720de615";
        private static readonly string luisEndpoint = "https://southeastasia.api.cognitive.microsoft.com";
        private static readonly LUISClient luisClient = new LUISClient(luisAppId, luisSubscriptionKey, luisEndpoint);

        private static readonly string sqlConnectionString = "Server=tcp:camella.database.windows.net,1433;Database=camelladb;Uid=camellaadmin@camella;Pwd=P@ssw0rd;Encrypt=yes;TrustServerCertificate=no;Connection Timeout=30;";
        private static readonly MandalaySAPClient sapClient = new MandalaySAPClient(sqlConnectionString);

        private readonly MandalayBotAccessors _accessors;
        private DialogSet _dialogs;

        /// <summary>
        /// Initializes a new instance of the <see cref="MandalayBot"/> class.
        /// </summary>
        /// <param name="botServices">Bot services.</param>
        /// <param name="accessors">Bot State Accessors.</param>
        public MandalayBot(BotServices services, MandalayBotAccessors accessors, ILoggerFactory loggerFactory)
        {
            _services = services ?? throw new ArgumentNullException(nameof(services));
            _accessors = accessors ?? throw new ArgumentNullException(nameof(accessors));

            // The DialogSet needs a DialogState accessor, it will call it when it has a turn context.
            _dialogs = new DialogSet(accessors.ConversationDialogState);
            var waterfallSteps = new WaterfallStep[]
            {
                LocationAsync,
                BudgetAsync,
                ResidentialTypeAsync,
                BedroomAsync,
                BathroomAsync
            };

            // Add named dialogs to the DialogSet. These names are saved in the dialog state.
            _dialogs.Add(new WaterfallDialog("details", waterfallSteps));
            _dialogs.Add(new ChoicePrompt("location"));
            _dialogs.Add(new NumberPrompt<double>("budget"));
            _dialogs.Add(new ChoicePrompt("residentialType"));
            _dialogs.Add(new ChoicePrompt("bedroom"));
            _dialogs.Add(new ChoicePrompt("bathroom"));
            _dialogs.Add(new ConfirmPrompt("confirm"));
        }


        /// <summary>
        /// Run every turn of the conversation. Handles orchestration of messages.
        /// </summary>
        /// <param name="context">Bot Turn Context.</param>
        /// <param name="cancellationToken">Task CancellationToken.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        public async Task OnTurnAsync(ITurnContext context, CancellationToken cancellationToken)
        {
            var activity = context.Activity;
            var conversation = new List<string>();

            if (context.Activity.Type == ActivityTypes.ConversationUpdate) // Greet when users are added to the conversation.
            {
                if (context.Activity.MembersAdded.Any())
                {
                    // Iterate over all new members added to the conversation
                    foreach (var member in context.Activity.MembersAdded)
                    {
                        // Greet anyone that was not the target (recipient) of this message
                        // the 'bot' is the recipient for events from the channel,
                        // turnContext.Activity.MembersAdded == turnContext.Activity.Recipient.Id indicates the
                        // bot was added to the conversation.
                        if (member.Id != context.Activity.Recipient.Id)
                        {
                            await context.SendActivityAsync("Hi there! I'm Esang - how can I help you today?");
                        }
                    }
                }
            }
            else if (activity.Type == ActivityTypes.Message)
            {
                var query = activity.Text;
                conversation.Add($"PERSON: {query}");

                // The Application ID from any published app in luis.ai, found in Manage > Application Information 
                var luisResult = await luisClient.GetIntentAsync(query);
                //await turnContext.SendActivityAsync($"Top scoring intent: {luisResult.TopScoringIntent.Name}  ({luisResult.TopScoringIntent.Score})");

                if (luisResult.TopScoringIntent.Name == Inquiry)
                {
                    //await turnContext.SendActivityAsync("Starting the sales process...");
                    // Run the DialogSet - let the framework identify the current state of the dialog from
                    // the dialog stack and figure out what (if any) is the active dialog.
                    var dialogContext = await _dialogs.CreateContextAsync(context, cancellationToken);
                    var results = await dialogContext.ContinueDialogAsync(cancellationToken);

                    // If the DialogTurnStatus is Empty we should start a new dialog.
                    if (results.Status == DialogTurnStatus.Empty)
                    {
                        await dialogContext.BeginDialogAsync("details", null, cancellationToken);
                    }
                    else if (results.Status == DialogTurnStatus.Complete)
                    {
                        //var bedroom = results.Reslt["bathroom"];
                        //var res = (SalesInquiry)results.Result;
                        await context.SendActivityAsync("Thank you. Searching for properties that match your requirements...");

                        var replies = sapClient.GetPropertiesAsync(0, 0, "Manila", "Vertical").Result
                            .Take(3)
                            .Select(p =>
                            {
                                var reply = context.Activity.CreateReply();
                                var card = new HeroCard(p.XWETEXT, p.Price, p.HSE_M);
                                new CardAction(ActionTypes.OpenUrl, "Learn More", value: "https://www.camella.com.ph/properties/property/house-and-lot/camella-aklan");
                                card.Images = new List<CardImage> { new CardImage(url: "https://www.camella.com.ph/assets/uploads/2018/08/2018_Camella_Property-for-Sale_Camella_Aklan-Gallery_(1)1.jpg") };
                                reply.Attachments.Add(card.ToAttachment());
                                return reply;
                            });

                        await context.SendActivitiesAsync(replies.ToArray());


                        #region sample code
                        //// use SAP API here instead of fake property
                        //var reply = context.Activity.CreateReply();
                        //var card = new HeroCard("Camella Aklan", "1,950,000 PHP");
                        //card.Text = "Less than an hour away from the world famous lifestyle destination Boracay, Camella Aklan is located within the progressive Metro Kalibo. Camella Aklan is a sprawling 11-hectare, Italian-inspired house and lot development in Numancia.";

                        //card.Buttons = new List<CardAction>
                        //{
                        //     new CardAction(ActionTypes.OpenUrl, "Learn More", value: "https://www.camella.com.ph/properties/property/house-and-lot/camella-aklan"),
                        //};

                        //card.Images = new List<CardImage>
                        //{
                        //    new CardImage(url: "https://www.camella.com.ph/assets/uploads/2018/08/2018_Camella_Property-for-Sale_Camella_Aklan-Gallery_(1)1.jpg"),
                        //};


                        //reply.Attachments.Add(card.ToAttachment());
                        //await context.SendActivityAsync(reply);


                        ////youtube example
                        //var videoMsg = context.Activity.CreateReply();
                        //videoMsg.Attachments.Add(new VideoCard("My Video", "a subtitle", "some text", media: new[] { new MediaUrl(@"http://download.blender.org/peach/bigbuckbunny_movies/BigBuckBunny_320x180.mp4") }).ToAttachment());
                        //await context.SendActivityAsync(videoMsg); 
                        #endregion
                    }

                    // Save the dialog state into the conversation state.
                    await _accessors.ConversationState.SaveChangesAsync(context, false, cancellationToken);

                    // Save the user profile updates into the user state.
                    await _accessors.UserState.SaveChangesAsync(context, false, cancellationToken);


                    //lookup SAP for recommended property(ies)


                    //prompt user whether they want to purchase the house
                    //if yes, trigger FormFlow for user details
                    //submit complete data to SAP
                }
                else
                {
                    //send question to QnA Maker

                    // get answers that are not chitchat
                    var results = qna.GetAnswer(query);
                    var result = results.Answers
                        .Where(a => a.Id != -1)
                        .Where(a => a.Metadata.Where(m => m.Value == "chitchat").Count() == 0)
                        .OrderByDescending(a => a.Score)
                        .ToList();

                    // display response based on API response
                    if (result.Count == 0)
                    {
                        var message = "I'm sorry, I don't know the answer to this.";
                        await context.SendActivityAsync(message);
                        conversation.Add($"BOT: {message}");
                    }
                    else if (result.FirstOrDefault().Score < 0.5)
                    {
                        // return choice dialog
                        await context.SendActivityAsync("I'm not 100% sure what you mean, does this answer your question?");
                    }
                    else
                    {
                        await context.SendActivityAsync(result.FirstOrDefault().AnswerText);
                    }
                }
            }
        }

        private static List<Choice> areaChoices = new List<Choice>() { new Choice("Manila"), new Choice("Makati") };
        private static async Task<DialogTurnResult> LocationAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
            => await stepContext.PromptAsync("location", new PromptOptions { Prompt = MessageFactory.Text("Which area do you prefer?"), Choices = areaChoices }, cancellationToken);

        private static async Task<DialogTurnResult> BudgetAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
            => await stepContext.PromptAsync("budget", new PromptOptions { Prompt = MessageFactory.Text("How much is your budget?") }, cancellationToken);

        private static async Task<DialogTurnResult> ResidentialTypeAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
            => await stepContext.PromptAsync("residentialType", new PromptOptions { Prompt = MessageFactory.Text("What type of residential do you prefer?"), Choices = new List<Choice>() { new Choice("Vertical"), new Choice("Horizontal") } }, cancellationToken);

        private static List<Choice> roomChoices = new List<Choice>() { new Choice("1"), new Choice("2"), new Choice("3"), new Choice("4") };
        private static async Task<DialogTurnResult> BedroomAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
            => await stepContext.PromptAsync("bedroom", new PromptOptions { Prompt = MessageFactory.Text("How many bedrooms?"), Choices = roomChoices }, cancellationToken);

        private static async Task<DialogTurnResult> BathroomAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
            => await stepContext.PromptAsync("bathroom", new PromptOptions { Prompt = MessageFactory.Text("How many bathrooms?"), Choices = roomChoices }, cancellationToken);


    }
}
