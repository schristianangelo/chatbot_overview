﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mandalay.Bot.Models
{
    public class SalesInquiry
    {
        public string Location { get; set; }
        public double Budget { get; set; }
        public string ResidentialType { get; set; }
        public int Bathrooms { get; set; }
        public int Bedrooms { get; set; }
    }
}
