﻿using Mandalay.LUIS.Models;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace Mandalay.LUIS
{
    public class LUISClient
    {
        private string _luisAppId, _luisSubscriptionKey, _luisEndpoint;
        private HttpClient httpClient;

        public LUISClient(string luisAppid, string luisSubscriptionKey, string luisEndpoint)
        {
            _luisAppId = luisAppid;
            _luisSubscriptionKey = luisSubscriptionKey;
            _luisEndpoint = luisEndpoint;

            httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", luisSubscriptionKey);
            //httpClient.BaseAddress = new Uri($"/*{luisEndpoint}/{luisAppid}*/");
        }

        public async Task<LUISResult> GetIntentAsync(string query)
        {
            var req = await httpClient.GetAsync($"{_luisEndpoint}/luis/v2.0/apps/{_luisAppId}/?verbose=true&q={query}");
            var resp = await req.Content.ReadAsStringAsync();
            var obj = JsonConvert.DeserializeObject<LUISResult>(resp);
            return obj;
        }
    }
}
