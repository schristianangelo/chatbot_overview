﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mandalay.LUIS.Models
{
    public class Intent
    {
        [JsonProperty("Intent")]
        public string Name { get; set; }
        public double Score { get; set; }

    }
}
