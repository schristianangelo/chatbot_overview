﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mandalay.LUIS.Models
{
    public class LUISResult
    {
        public string Query { get; set; }
        [JsonIgnore]
        public Intent TopScoringIntent => Intents.OrderByDescending(i => i.Score).First();
        public List<Intent> Intents { get; set; } = new List<Intent>();
    }
}
