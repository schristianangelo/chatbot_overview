﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mandalay.QnAMaker.Models
{
    public class Answer
    {
        public List<string> Questions { get; set; }
        [JsonProperty("answer")]
        public string AnswerText { get; set; }
        public double Score { get; set; }
        public int Id { get; set; }
        public string Source { get; set; }
        public List<Metadata> Metadata { get; set; }

    }
}
