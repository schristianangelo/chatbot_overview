﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Mandalay.QnAMaker.Models
{
    public class AnswerResponse
    {
        [JsonProperty("answers")]
        public List<Answer> Answers { get; set; }
    }
}
