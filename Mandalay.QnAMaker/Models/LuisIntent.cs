﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mandalay.QnAMaker.Models
{
    public class LuisIntent
    {
        public string Intent { get; set; }
        public double Score { get; set; }
    }
}
