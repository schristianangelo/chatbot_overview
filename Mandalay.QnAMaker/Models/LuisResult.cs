﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mandalay.QnAMaker.Models
{
    public class LuisResult
    {
        public string Query { get; set; }
        public LuisIntent TopScoringIntent { get; set; }
        public List<LuisIntent> Intents { get; set; }
    }
}
