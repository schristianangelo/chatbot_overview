﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mandalay.QnAMaker.Models
{
    public class Metadata
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
