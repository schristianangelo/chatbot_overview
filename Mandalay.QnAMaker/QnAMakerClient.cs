﻿using Mandalay.QnAMaker.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace Mandalay.QnAMaker
{
    public class QnAMakerClient
    {
        string _apiBaseUrl, _apiKey, _knowledgeBaseId;

        public QnAMakerClient(string apiBaseUrl, string apiKey, string knowledgeBaseId)
        {
            _apiBaseUrl = apiBaseUrl;
            _apiKey = apiKey;
            _knowledgeBaseId = knowledgeBaseId;
        }

        public AnswerResponse GetAnswer(string question)
        {
            var payload = JsonConvert.SerializeObject(new QuestionRequest { question = question });

            var url = $"{_apiBaseUrl}/qnamaker/knowledgebases/{_knowledgeBaseId}/generateAnswer";

            var http = (HttpWebRequest)WebRequest.Create(new Uri(url));
            http.Accept = "application/json";
            http.ContentType = "application/json";
            http.Method = "POST";
            http.Headers.Add("Authorization", $"EndpointKey {_apiKey}");

            var encoding = new ASCIIEncoding();
            var bytes = encoding.GetBytes(payload);

            using (var newStream = http.GetRequestStream())
            {
                newStream.Write(bytes, 0, bytes.Length);
                newStream.Close();

                var response = http.GetResponse();

                using (var stream = response.GetResponseStream())
                {
                    var sr = new StreamReader(stream);
                    var content = sr.ReadToEnd();
                    var ret = JsonConvert.DeserializeObject<AnswerResponse>(content);
                    return ret;
                }
            }
        }
    }
}
