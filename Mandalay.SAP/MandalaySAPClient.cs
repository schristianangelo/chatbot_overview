﻿using Mandalay.SAP.Models;
using System;
using System.Data.SqlClient;
using Dapper;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mandalay.SAP
{
    public class MandalaySAPClient
    {
        string _connectionString;

        public MandalaySAPClient(string connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<List<Property>> GetPropertiesAsync(int minBedrooms, int minBathrooms, string area, string type)
        {
            //var query = $"select * from properties where bedrooms >= {minBedrooms} and bathrooms >= {minBathrooms}";
            var query = $"select top 10 * from SALES_UNIT";
            using (var sql = new SqlConnection(_connectionString))
            {
                var properties = sql.Query<Property>(query).ToList();
                return properties;
            }
        }
    }
}
