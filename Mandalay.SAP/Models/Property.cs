﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mandalay.SAP.Models
{
    public class Property
    {
        public int ID { get; set; }
        public string REFNO { get; set; }
        public string LOTSIZE { get; set; }
        public string STAT_DESC { get; set; }
        public string XMBEZ { get; set; }
        public string Price { get; set; }
        public string MCC { get; set; }
        public string VAT { get; set; }
        public string NTCP { get; set; }
        public string LOT_PRICE { get; set; }
        public string XWETEXT { get; set; }
        public string BUTXT { get; set; }
        public string STRAS { get; set; }
        public string CITY { get; set; }
        public string BRAND { get; set; }
        public string HSE_M { get; set; }

    }
}
